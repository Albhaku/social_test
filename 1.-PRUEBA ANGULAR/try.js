(function(){
    var app = angular.module("socialNet", [ ]);

    app.controller("friendController", function(){
        this.userInfo = users;
    });
    var users = [
        {
            name:"Alberto",
            age: 20,
            location: "México",
            money: 1234,
            add: true,
            friend: false,
            images: [
                "guser-3.png",
                "guser.png",
                "guser-2.png"
            ]
        },
        {
            name:"Hector",
            age: 25,
            location: "México",
            money: 1234,
            add: true,
            friend: false,
            images: [
                "guser-2.png",
                "guser-3.png",
                "guser.png"
            ]
        },
        {
            name:"Jail",
            age: 25,
            location: "México",
            money: 1234,
            add: true,
            friend: false,
            images: [
                "guser-3.png",
                "guser-2.png",
                "guser.png"
            ]
        },
        {
            name:"Leo",
            age: 25,
            location: "México",
            money: 1234,
            add: true,
            friend: false,
            images: [
                "guser.png",
                "guser-2.png",
                "guser-3.png"
            ]
        },
        {
            name:"Anali",
            age: 25,
            location: "México",
            money: 1234,
            add: true,
            friend: false,
            images: [
                
            ]
        }
    ];

})();
